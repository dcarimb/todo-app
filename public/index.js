
async function main() {
    console.log("running main")
    var divApp=document.getElementById("app");
    var title=document.createElement('h1');
    title.textContent="ToDo app";
    divApp.appendChild(title);

    var todosResponse = await fetch('http://localhost:8080/api/todos');
    var todosJson = await todosResponse.json();

    console.log(todosJson);
}

// stessa roba di
// document.addEventListener('DOMContentLoaded', main)
document.addEventListener('DOMContentLoaded', function() {
    main();
})