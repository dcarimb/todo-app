<?php

namespace App\Http\Controllers;

// dependencies that allow the use of Request and Response within methods
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TodoController extends Controller {

    public function add(Request $request) {
        $description = $request->input('description');

        $results = app('db')->insert(
            // 'cuiri' (query by strano)
            "INSERT INTO tasks(description, done, insertDate) values('$description', false, now())" 
        );
        return new Response(null, 201);
    }

    public function update(Request $request, $id) {

        $description = $request->input('description');
        $done = $request->input('done');

        // strano is done with donne

        $results = app('db')->update( 
            // update taskxs  
            "UPDATE tasks SET description='$description', done=$done WHERE id='$id'" 
        );
        return new Response(null, 200);

        // di mauro is driving strano craiziiiiii
    }

    public function delete(Request $request, $id) {

        $results = app('db')->delete(
            "DELETE from tasks WHERE id='$id'" 
        );
        return new Response(null, 204);

    }

}

?>