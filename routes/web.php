<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/strano', function () use ($router) {
    return "Strano è strano";
});

$router->get('/strano/manuel', function () use ($router) {
    return "Strano è manuel";
});

$router->get('/api/todos', function () use ($router) {
    // upload array of todo
    $results = app('db')->select("SELECT * FROM tasks");
    return $results;
});

// insert
// calls method add of TodoController like TodoController->add(..params)
$router->post('/api/todos', 'TodoController@add');

// id needed as param for update method
$router->put('/api/todos/{id}', 'TodoController@update');

$router->delete('/api/todos/{id}', 'TodoController@delete');